class: Workflow
cwlVersion: v1.0
id: solarimaging
label: solarimaging
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
inputs:
  - id: calibrator
    type: 'Directory[]'
    'sbg:x': -498.1015319824219
    'sbg:y': 156.31472778320312
  - id: sun
    type: 'Directory[]'
    'sbg:x': -512.3553466796875
    'sbg:y': 425.75128173828125
  - id: sky_model
    'sbg:x': -490.72222900390625
    'sbg:y': 17.29166603088379
  - id: sources
    type: 'string[]'
    'sbg:x': -511.42132568359375
    'sbg:y': 304.5245056152344
outputs:
  - id: image
    outputSource:
      - wsclean/image
    type: File
    'sbg:x': 1132.7830810546875
    'sbg:y': 81.57866668701172
  - id: dirty_image
    outputSource:
      - wsclean/dirty_image
    type: File
    'sbg:x': 1145.4735107421875
    'sbg:y': 294.7766418457031
steps:
  - id: dp3_execute
    in:
      - id: msin
        source:
          - msin
          - calibrator
      - id: steps
        source:
          - flagedge_calibrator/augmented_steps
      - id: autoweight
        default: true
    out:
      - id: secondary_output_files
      - id: secondary_output_directories
      - id: msout
    run: ../lofar-cwl/steps/DP3.Execute.cwl
    label: DP3.Execute
    scatter:
      - msin
    'sbg:x': -123
    'sbg:y': -118
  - id: flagedge_calibrator
    in: []
    out:
      - id: augmented_steps
    run: ../lofar-cwl/steps/DP3.PreflaggerStepGenerator.cwl
    'sbg:x': -330
    'sbg:y': -244
  - id: predict
    in:
      - id: msin
        source: dp3_execute/msout
      - id: sources_db
        source: make_sourcedb/sourcedb
      - id: sources
        source:
          - sources
    out:
      - id: msout
    run: ../lofar-cwl/steps/predict.cwl
    scatter:
      - msin
    'sbg:x': 133
    'sbg:y': -92
  - id: h5parm_collector
    in:
      - id: h5parmFiles
        source:
          - gaincal/h5parm
    out:
      - id: outh5parm
      - id: log
    run: ../lofar-cwl/steps/H5ParmCollector.cwl
    label: H5parm_collector
    'sbg:x': 433
    'sbg:y': -50
  - id: make_sourcedb
    in:
      - id: sky_model
        source: sky_model
    out:
      - id: sourcedb
      - id: log
    run: ../lofar-cwl/steps/makesourcedb.cwl
    label: make_sourcedb_ateam
    'sbg:x': -15.88888931274414
    'sbg:y': -298.60888671875
  - id: flagedge_sun
    in: []
    out:
      - id: augmented_steps
    run: ../lofar-cwl/steps/DP3.PreflaggerStepGenerator.cwl
    'sbg:x': -13.472081184387207
    'sbg:y': 111.53807067871094
  - id: dp3_execute_1
    in:
      - id: msin
        source: sun
      - id: steps
        source:
          - flagedge_sun/augmented_steps
      - id: autoweight
        default: true
    out:
      - id: secondary_output_files
      - id: secondary_output_directories
      - id: msout
    run: ../lofar-cwl/steps/DP3.Execute.cwl
    label: DP3.Execute
    scatter:
      - msin
    'sbg:x': 449.2081298828125
    'sbg:y': 190.84771728515625
  - id: applycal
    in:
      - id: msin
        source: dp3_execute_1/msout
      - id: parmdb
        source: h5parm_collector/outh5parm
    out:
      - id: msout
    run: ../../lofar-cwl/steps/applycal.cwl
    'sbg:x': 721.1517333984375
    'sbg:y': 176.82852172851562
  - id: gaincal
    in:
      - id: msin
        source: predict/msout
      - id: caltype
        default: diagonal
      - id: sourcedb
        source: make_sourcedb/sourcedb
      - id: usebeammodel
        default: true
      - id: solint
        default: 4
      - id: onebeamperpatch
        default: true
    out:
      - id: msout
      - id: h5parm
    run: ../../lofar-cwl/steps/gaincal.cwl
    scatter:
      - msin
    'sbg:x': 299
    'sbg:y': -102
  - id: wsclean
    in:
      - id: msin
        source: applycal/msout
    out:
      - id: dirty_image
      - id: image
    run: ../../lofar-cwl/steps/wsclean.cwl
    label: WSClean
    'sbg:x': 976.1167602539062
    'sbg:y': 188.5736083984375
requirements:
  - class: SubworkflowFeatureRequirement
  - class: ScatterFeatureRequirement
  - class: MultipleInputFeatureRequirement
