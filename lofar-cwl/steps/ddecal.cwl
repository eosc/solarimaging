#!/usr/bin/env cwl-runner

class: CommandLineTool
cwlVersion: v1.0
id: ddecal
baseCommand: [DPPP]

requirements:
  InlineJavascriptRequirement: {}

hints:
  DockerRequirement:
    dockerPull: lofareosc/lofar-pipeline-ci:latest

arguments:
  - steps=[ddecal]
  - ddecal.mode=rotation+diagonal
  - ddecal.uvlambdamin=300
  - ddecal.maxiter=50
  - ddecal.nchan=1
  - ddecal.solint=1
  - ddecal.propagateconvergedonly=True
  - ddecal.flagdivergedonly=True
  - ddecal.tolerance=1.e-3
  - ddecal.usemodelcolumn=True


inputs:
  - id: msin
    type: Directory?
    doc: Input Measurement Set
    inputBinding:
        prefix: msin=
        separate: false

  - id: msin_datacolumn
    type: string
    default: DATA
    doc: Input data Column
    inputBinding:
        prefix: msin.datacolumn=
        separate: false

  - id: msin_modelcolum
    type: string
    default: MODEL_DATA
    doc: Model data Column
    inputBinding:
        prefix: msin.modelcolumn=
        separate: false

  - id: output_name_h5parm
    type: string
    default: instrument.h5
    inputBinding:
        prefix: ddecal.h5parm=
        separate: false
  - id: msout_name
    type: string
    doc: Output Measurement Set
    default: out.MS
    inputBinding:
        prefix: msout=
        separate: false

#--------------------
  - id: propagate_solutions
    type: boolean
    default: true
    inputBinding:
      prefix: propagatesolutions=True
  - id: flagunconverged
    type: boolean
    default: false
    doc: |
      Flag unconverged solutions (i.e., those from solves that did not converge
      within maxiter iterations).
    inputBinding:
      prefix: flagdivergedonly=True
  - id: flagdivergedonly
    default: false
    type: boolean
    doc: |
      Flag only the unconverged solutions for which divergence was detected.
      At the moment, this option is effective only for rotation+diagonal
      solves, where divergence is detected when the amplitudes of any station
      are found to be more than a factor of 5 from the mean amplitude over all
      stations.
      If divergence for any one station is detected, all stations are flagged
      for that solution interval. Only effective when flagunconverged=true
      and mode=rotation+diagonal.
    inputBinding:
      prefix: flagdivergedonly=True
outputs:
  - id: msout
    doc: Output Measurement Set
    type: Directory
    outputBinding:
      glob: $(inputs.msout_name=="."?inputs.msin:inputs.msout_name)

  - id: h5parm
    doc: Filename of output H5Parm (to be read by e.g. losoto)
    type: File
    format: lofar:#H5Parm
    outputBinding:
      glob: $(inputs.output_name_h5parm)

$namespaces:
  lofar: https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl
$schema:
  - https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl
