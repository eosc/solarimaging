#!/usr/bin/env cwl-runner

class: CommandLineTool
cwlVersion: v1.0
id: gaincal
baseCommand: [DPPP]

requirements:
  InlineJavascriptRequirement: {}

hints:
  DockerRequirement:
    dockerPull: lofareosc/lofar-pipeline-ci:latest

arguments:
  - steps=[gaincal]


inputs:
  - id: msin
    type: Directory?
    doc: Input Measurement Set
    inputBinding:
        prefix: msin=
        separate: false
  - id: caltype
    doc: |
      The type of calibration that needs to be performed.
    type:
        type: enum
        symbols:
            - diagonal
            - fulljones
            - phaseonly
            - scalarphase
            - amplitude
            - scalaramplitude
            - tec
            - tecandphase
    inputBinding:
        prefix: gaincal.caltype=
        separate: false
  - id: sourcedb
    type: File
    inputBinding:
        prefix: gaincal.sourcedb=
        separate: false
  - id: usebeammodel
    type: boolean
    default: true
    inputBinding:
        prefix: gaincal.usebeammodel=true
        separate: false
  - id: solint
    type: int
    default: 1
    doc: |
        Number of time slots on which a solution is assumed to be constant (same as CellSize.Time in BBS).
        0 means all time slots. Note that for larger settings of solint, and specially for solint = 0,
        the memory usage of gaincal will be large (all visibilities for a solint should fit in memory).
  - id: msin_datacolumn
    type: string?
    default: DATA
    doc: Input data Column
    inputBinding:
        prefix: msin.datacolumn=
        separate: false
  - id: onebeamperpatch
    type: boolean
    doc: Input data Column
    default: true
    inputBinding:
        prefix: gaincal.onebeamperpatch=True
        separate: false
  - id: msin_modelcolum
    type: string
    default: MODEL_DATA
    doc: Model data Column
    inputBinding:
        prefix: msin.modelcolumn=
        separate: false

  - id: output_name_h5parm
    type: string
    default: instrument.h5
    inputBinding:
        prefix: gaincal.parmdb=
        separate: false
  - id: msout_name
    type: string
    doc: Output Measurement Set
    default: out.MS
    inputBinding:
        prefix: msout=
        separate: false

outputs:
  - id: msout
    doc: Output Measurement Set
    type: Directory
    outputBinding:
      glob: $(inputs.msout_name=="."?inputs.msin:inputs.msout_name)

  - id: h5parm
    doc: Filename of output H5Parm (to be read by e.g. losoto)
    type: File
    format: lofar:#H5Parm
    outputBinding:
      glob: $(inputs.output_name_h5parm)

$namespaces:
  lofar: https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl
$schema:
  - https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl
