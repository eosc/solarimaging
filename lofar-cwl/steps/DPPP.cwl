class: CommandLineTool
cwlVersion: v1.0
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
id: dppp
baseCommand:
  - DPPP
inputs:
  - id: parset
    type: File?
    inputBinding:
      position: -1
  - id: msin
    type: Directory?
    inputBinding:
      position: 0
      prefix: msin=
      separate: false
    doc: Input Measurement Set
  - default: out.MS
    id: msout_name
    type: string
    inputBinding:
      position: 0
      prefix: msout=
      separate: false
    doc: Output Measurement Set
  - id: secondary_files
    type: 'File[]?'
    doc: Secondary files needed for the step
  - id: secondary_directories
    type: 'Directory[]?'
    doc: Secondary directories needed for the step
  - id: output_file_names
    type: Any
    doc: Expected output file names
  - id: output_directory_names
    type: Any
    doc: Expected output file names
  - id: autoweight
    type: boolean
    default: true
    inputBinding:
      prefix: -msin.autoweight=True
outputs:
  - id: msout
    doc: Output Measurement Set
    type: Directory
    outputBinding:
      glob: '$(inputs.msout_name=="."?inputs.msin:inputs.msout_name)'
  - id: secondary_output_files
    doc: Secondary output files
    type: Any
    outputBinding:
      outputEval: |-
        ${
          var output={}
          for(var step_name in inputs.output_file_names){
            var file_per_idx = inputs.output_file_names[step_name];
            for(var file_idx in file_per_idx){
                var file_name = file_per_idx[file_idx];

                output[file_idx] = {'class':'File', 'path': file_name};
            }
          }
          return output
        }
  - id: secondary_output_directories
    doc: Secondary output directories
    type: Any
    outputBinding:
      outputEval: |-
        ${
          var output={}
          for(var step_name in inputs.output_directory_names){
            var file_per_idx = inputs.output_directory_names[step_name];
            for(var file_idx in file_per_idx){
                var file_name = file_per_idx[file_idx];

                output[file_idx] = {'class':'Directory', 'path': file_name};
            }
          }
          return output
        }
  - id: logfile
    type: File?
    outputBinding:
      glob: DPPP.log
hints:
  - class: DockerRequirement
    dockerPull: lofareosc/lofar-pipeline
stdout: DPPP.log
requirements:
  - class: InlineJavascriptRequirement
