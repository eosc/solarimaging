class: ExpressionTool
cwlVersion: v1.0
id: dirselector
inputs:
  - id: inputs
    type: Any
    doc: input files
  - id: step_name
    type: string
    doc: >-
      id of the step that generates the output file or directory
      (e.g. 'demixstepgenerator')
  - id: key_name
    type: string
    doc: >-
      key of the parset that contains the name of the outputfile
      (e.g. 'instrumentmodel')
outputs:
  - id: output
    type: [Directory, 'Directory[]']

expression: |
  ${
    var step_name = inputs.step_name
    var key_name = inputs.key_name

    if(inputs.inputs.hasOwnProperty(step_name)){
       var inputs_per_step = inputs.inputs[step_name]
       if(inputs_per_step.hasOwnProperty(key_name)){
         return {'output': inputs_per_step[key_name]}
       }
    }
    return {'output': []}

  }
label: DirSelector

requirements:
  - class: InlineJavascriptRequirement
