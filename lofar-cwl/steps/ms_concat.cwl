class: CommandLineTool
cwlVersion: v1.0
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
id: ms_concat


baseCommand:
  - concat_MS.py
inputs:
  - id: min_length
    type: int?
    default: 50
    inputBinding:
      position: 0
      prefix: '--min_length'
    doc: Minimum amount of subbands to concatenate in frequency.
  - id: overhead
    type: float?
    inputBinding:
      position: 0
      prefix: '--overhead'
    doc: |
      Only use this fraction of the available memory for deriving the amount
      of data to be concatenated.
  - id: msin
    type: 'Directory[]'
    inputBinding:
      position: 1
      shellQuote: false
  - default: out.MS
    id: msout
    type: string
    inputBinding:
      position: 2
outputs:
  - id: concat_meta_ms
    type: Directory[]
    outputBinding:
      glob: >
        $(inputs.msout)_[0-9*]
  - id: concat_additional_ms
    type: Directory[]
    outputBinding:
      glob: $(inputs.msout)_*_CONCAT
  - id: ms_outs
    type: Directory[]
    outputBinding:
      outputEval: $(inputs.msin)
label: ms_concat
requirements:
  - class: ShellCommandRequirement
  - class: DockerRequirement
    dockerPull: lofareosc/lofar-pipeline-ci:latest
  - class: InlineJavascriptRequirement
  - class: InitialWorkDirRequirement
    listing:
      - entry: $(inputs.msin)
        writable: true
