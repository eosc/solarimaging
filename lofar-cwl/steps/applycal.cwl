class: CommandLineTool
cwlVersion: v1.0
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
id: applycal
baseCommand:
  - DPPP
arguments:
  - steps=[applycal]
inputs:
  - id: msin
    type: Directory
    inputBinding:
      position: 0
      prefix: msin=
      separate: false
    doc: Input Measurement Set
  - default: DATA
    id: msin_datacolumn
    type: string
    inputBinding:
      position: 0
      prefix: msin.datacolumn=
      separate: false
    doc: Input data Column
  - id: parmdb
    type: File
    doc: Path of parmdb in which the parameters are stored. This can also be an H5Parm file, in that case the filename has to end in '.h5'
    inputBinding:
      prefix: applycal.parmdb
  - id: correction
    default: gain
    type:
      type: enum
      symbols:
        - gain
        - tec
        - clock
        - rotationangle
        - rotation
        - scalarphase
        - scalaramplitude
        - rotationmeasure
        - fulljones
    doc: | 
        Type of correction to perform. When using H5Parm, this is for now the name of the soltab; the type will be deduced from the metadata in that soltab, except for full Jones, in which case correction should be 'fulljones'.
    inputBinding:
        prefix: applycal.correction=
  
outputs:
  - id: msout
    doc: Output Measurement Set
    type: Directory
    outputBinding:
        glob: $(inputs.msin.basename)
      
requirements:
  - class: InitialWorkDirRequirement
    listing:
        - entry: $(inputs.msin)
          writable: true
  - class: InlineJavascriptRequirement
hints:
  - class: DockerRequirement
    dockerPull: lofareosc/lofar-pipeline-ci:latest
