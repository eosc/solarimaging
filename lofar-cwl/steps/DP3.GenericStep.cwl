#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: ExpressionTool
id: generic_step

requirements:
  - class: InlineJavascriptRequirement

inputs:
  - id: steps
    type: Any[]

  - id: parameters
    type: Any?

outputs:
  - id: augmented_steps
    type: Any[]
expression: |-
  ${
   var step_type = inputs.parameters.step_type;
   var step_id = inputs.parameters.step_id;

   var output_file_type = inputs.parameters.output_files;
   var output_files = {}

   for(var index in output_file_type){
     var file_type = output_file_type[index]
     if(file_type != undefined && file_type != null){
       output_files[file_type] = inputs.parameters[file_type]
     }
   }

   var input_secondary_files = inputs.parameters.input_secondary_files;

   delete inputs.parameters.step_type;
   delete inputs.parameters.step_id;
   delete inputs.parameters.steps;
   delete inputs.parameters.parameters;
   delete inputs.parameters.steps;


   var new_step = {'step_type': step_type,
                   'step_id': step_id,
                   'parameters': inputs.parameters,
                   'output_files': output_files};


   var in_parameters = inputs.steps;

   // Checks if the step_id is duplicate if so raised an exception
   var step_ids = []
   for(var step in in_parameters){
     step = in_parameters[step]
     step_ids.push(step.step_id)
   }
   if(step_ids.indexOf(step_id) >= 0) throw 'step_id has to be unique'

   var out_parameters =  in_parameters == null ? [new_step] : in_parameters.concat(new_step)

   return {'augmented_steps': out_parameters};
  }
