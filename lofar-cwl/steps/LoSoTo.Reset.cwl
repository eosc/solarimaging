#!/usr/bin/env cwl-runner

class: CommandLineTool
cwlVersion: v1.0
id: losoto_reset

doc: Subtract/divide two tables or a clock/tec/tec3rd/rm from a phase.

requirements:
  InlineJavascriptRequirement: {}
  InitialWorkDirRequirement:
    listing:
      - entryname: 'parset.config'
        entry: |
          [reset]
          soltab = $(inputs.soltab)
          operation = RESET
          $(inputs.dataVal !== null? 'dataVal=' + inputs.dataVal: '')

      - entryname: $(inputs.input_h5parm.basename)
        entry: $(inputs.input_h5parm)
        writable: true

baseCommand: "losoto"

arguments:
  - $(inputs.input_h5parm.basename)
  - parset.config

hints:
  DockerRequirement:
    dockerPull: lofareosc/lofar-pipeline-ci:latest

inputs:
  - id: input_h5parm
    type: File
    format: lofar:#H5Parm
  - id: soltab
    type: string
    doc: "Solution table"
  - id: dataVal
    type: float?
    doc: |
      If given set values to this number, otherwise uses 1 for amplitude and 0
      for all other soltab types.
outputs:
  - id: output_h5parm
    type: File
    format: lofar:#H5Parm
    outputBinding:
      glob: $(inputs.input_h5parm.basename)


$namespaces:
  lofar: https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl
$schema:
  - https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl
